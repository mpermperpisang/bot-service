package services

import (
	"net/http"
	"net/url"

	"gitlab.com/bot-service/helper"
	"gitlab.com/bot-service/helper/message"
)

/*Text : parameter text*/
var Text string

/*GetText : get parameter text*/
func GetText(params url.Values) error {
	if params.Get("text") != "" {
		Text = params.Get("text")
	} else {
		Text = "Hello, World!"
	}

	return nil
}

/*AssertStatusCode : validate status code response*/
func AssertStatusCode(response *http.Response) error {
	actualCode := response.StatusCode

	helper.AssertEqual(200, actualCode, message.ResponseCode(actualCode))

	return nil
}
