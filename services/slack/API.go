package slackbot

import (
	"bytes"
	"net/http"
	"net/url"
	"os"

	"gitlab.com/bot-service/helper"
	"gitlab.com/bot-service/services"
)

var webhook string
var response *http.Response
var body []byte

func getWebhook(params url.Values) error {
	if params.Get("webhook") != "" {
		webhook = params.Get("webhook")
	} else {
		webhook = os.Getenv("webhook_slack")
	}

	return nil
}

func getPayload() error {
	body = []byte(
		`{
			"text": "` + services.Text + `"
		}`)

	return nil
}

func hitPost(payload []byte) error {
	var err error

	response, err = http.Post(webhook, "application/json", bytes.NewBuffer(payload))
	helper.LogPanicln(err)

	return nil
}

/*PostMessage : post message to Slack without optional parameters*/
func PostMessage(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()

	getWebhook(params)
	services.GetText(params)
	getPayload()
	hitPost(body)
	services.AssertStatusCode(response)
}
