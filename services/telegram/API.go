package telegrambot

import (
	"net/http"
	"net/url"
	"os"

	"gitlab.com/bot-service/helper"
	"gitlab.com/bot-service/services"
)

var token, chatID string
var response *http.Response

func getToken(params url.Values) error {
	if params.Get("token") != "" {
		token = params.Get("token")
	} else {
		token = os.Getenv("token_telegram")
	}

	return nil
}

func getChatID(params url.Values) error {
	if params.Get("chatID") != "" {
		chatID = params.Get("chatID")
	} else {
		chatID = os.Getenv("chat_id_telegram")
	}

	return nil
}

func hitGet() error {
	var err error

	response, err = http.Get("https://api.telegram.org/bot" + token + "/sendMessage?chat_id=" + chatID + "&text=" + services.Text + "")
	helper.LogPanicln(err)

	return nil
}

/*SendMessage : send message to Telegram without optional parameters*/
func SendMessage(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()

	getToken(params)
	getChatID(params)
	services.GetText(params)
	hitGet()
	services.AssertStatusCode(response)
}
