package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"gitlab.com/bot-service/helper"
	slackbot "gitlab.com/bot-service/services/slack"
	telegrambot "gitlab.com/bot-service/services/telegram"
)

func init() {
	env := godotenv.Load()
	helper.LogPanicln(env)
}

func main() {
	r := mux.NewRouter()

	r.HandleFunc("/send-slack", slackbot.PostMessage).Methods(http.MethodPost)
	r.HandleFunc("/send-telegram", telegrambot.SendMessage).Methods(http.MethodPost)
	log.Fatal(http.ListenAndServe(":8282", r))
}
