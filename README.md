# bot-service

**Description**<br/>
Service to send chat using bot

**Install Package**<br/>
`$ make package`

**Running**<br/>
`$ make run`

**Contact**<br/>
`mpermperpisang@gmail.com`

**Reference**<br/>
- https://core.telegram.org/
- https://api.slack.com/
