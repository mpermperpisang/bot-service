package helper

import (
	"log"
)

/*AssertEqual : must equal*/
func AssertEqual(exp interface{}, act interface{}, err interface{}) error {
	if act != exp {
		log.Panicln(err)
	}

	return nil
}
