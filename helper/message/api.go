package message

import (
	"strconv"
)

/*ResponseCode pesan untuk response code*/
func ResponseCode(code int) string {
	return "Actual status code : " + strconv.Itoa(code)
}
